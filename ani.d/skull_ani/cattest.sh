#!/bin/bash
reps=0
until [[ $reps == 2 ]]
do
	count=1
	until [[ $count == 11 ]]
		do
			clear
			cat $count.ascii
			sleep 0.1
			count=$(($count+1))
		done
	reps=$(($reps+1))
done
count=1
until [[ $count == 12 ]]
do
	clear
	cat fin$count.ascii
	sleep 0.1
	count=$(($count+1))
done
clear
cat final.ascii
exit
