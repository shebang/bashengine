#!/bin/bash


USERNAME=root@pam
PASSWORD=yourpassword
VMID=198
NODE=proxmoxhost1
PROXY=proxmoxhost1.test.com


TICKET=`curl -k -d "username=$USERNAME&password=$PASSWORD"  https://$PROXY:8006/api2/json/access/ticket | sed 's/\\\\\//\//g' | sed 's/[{}]//g' | awk -v k="text" '{n=split($0,a,","); for (i=1; i<=n; i++) print a[i]}' | sed 's/\"\:\"/\|/g' | sed 's/[\,]/ /g' | sed 's/\"// g' | grep -w ticket |  awk -F "|" '{print $2}'`
echo $TICKET
curl -k -b "PVEAuthCookie=$TICKET" https://$PROXY:8006/api2/spiceconfig/nodes/$NODE/qemu/$VMID/spiceproxy?proxy=$PROXY > spiceproxy
remote-viewer spiceproxy
